﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Reflection;

namespace BarcodeWebAppContainer
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            string applicationDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
            string myFile = Path.Combine(applicationDirectory, "PIN.htm");
            barcodeEnabledWebBrowser1.Navigate(new Uri(myFile));
        }
    }
}