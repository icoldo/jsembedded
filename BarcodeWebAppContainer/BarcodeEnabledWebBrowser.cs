﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Symbol.Barcode2.Design;

namespace BarcodeWebAppContainer
{
    public class BarcodeEnabledWebBrowser : WebBrowser
    {
        #region Fields

        protected Symbol.Barcode2.Design.Barcode2 _barcodeService;
        private Dictionary<string, Action> _registeredActions;

        #endregion

        #region Initialization

        public BarcodeEnabledWebBrowser()
            : base()
        {
            InitializeComponent();
            InitializeRegisteredActions();
            Navigating += BarcodeEnabledWebBrowser_Navigating;
            Disposed += BarcodeEnabledWebBrowser_Disposed;
        }


        private void InitializeComponent()
        {
            this._barcodeService = new Symbol.Barcode2.Design.Barcode2();
            this.SuspendLayout();
            // 
            // barcodeService
            // 


            _barcodeService.OnScan += barcodeService_OnScan;

            this._barcodeService.Config.DecoderParameters.CODABAR = Symbol.Barcode2.Design.DisabledEnabled.Default;
            this._barcodeService.Config.DecoderParameters.CODABARParams.ClsiEditing = false;
            this._barcodeService.Config.DecoderParameters.CODABARParams.NotisEditing = false;
            this._barcodeService.Config.DecoderParameters.CODABARParams.Redundancy = true;
            this._barcodeService.Config.DecoderParameters.CODE128 = Symbol.Barcode2.Design.DisabledEnabled.Default;
            this._barcodeService.Config.DecoderParameters.CODE128Params.EAN128 = true;
            this._barcodeService.Config.DecoderParameters.CODE128Params.ISBT128 = true;
            this._barcodeService.Config.DecoderParameters.CODE128Params.Other128 = true;
            this._barcodeService.Config.DecoderParameters.CODE128Params.Redundancy = false;
            this._barcodeService.Config.DecoderParameters.CODE39 = Symbol.Barcode2.Design.DisabledEnabled.Default;
            this._barcodeService.Config.DecoderParameters.CODE39Params.Code32Prefix = false;
            this._barcodeService.Config.DecoderParameters.CODE39Params.Concatenation = false;
            this._barcodeService.Config.DecoderParameters.CODE39Params.ConvertToCode32 = false;
            this._barcodeService.Config.DecoderParameters.CODE39Params.FullAscii = false;
            this._barcodeService.Config.DecoderParameters.CODE39Params.Redundancy = false;
            this._barcodeService.Config.DecoderParameters.CODE39Params.ReportCheckDigit = false;
            this._barcodeService.Config.DecoderParameters.CODE39Params.VerifyCheckDigit = false;
            this._barcodeService.Config.DecoderParameters.CODE93 = Symbol.Barcode2.Design.DisabledEnabled.Default;
            this._barcodeService.Config.DecoderParameters.CODE93Params.Redundancy = false;
            this._barcodeService.Config.DecoderParameters.D2OF5 = Symbol.Barcode2.Design.DisabledEnabled.Default;
            this._barcodeService.Config.DecoderParameters.D2OF5Params.Redundancy = true;
            this._barcodeService.Config.DecoderParameters.EAN13 = Symbol.Barcode2.Design.DisabledEnabled.Default;
            this._barcodeService.Config.DecoderParameters.EAN8 = Symbol.Barcode2.Design.DisabledEnabled.Default;
            this._barcodeService.Config.DecoderParameters.EAN8Params.ConvertToEAN13 = false;
            this._barcodeService.Config.DecoderParameters.I2OF5 = Symbol.Barcode2.Design.DisabledEnabled.Default;
            this._barcodeService.Config.DecoderParameters.I2OF5Params.ConvertToEAN13 = false;
            this._barcodeService.Config.DecoderParameters.I2OF5Params.Redundancy = true;
            this._barcodeService.Config.DecoderParameters.I2OF5Params.ReportCheckDigit = false;
            this._barcodeService.Config.DecoderParameters.I2OF5Params.VerifyCheckDigit = Symbol.Barcode2.Design.I2OF5.CheckDigitSchemes.Default;
            this._barcodeService.Config.DecoderParameters.KOREAN_3OF5 = Symbol.Barcode2.Design.DisabledEnabled.Default;
            this._barcodeService.Config.DecoderParameters.KOREAN_3OF5Params.Redundancy = true;
            this._barcodeService.Config.DecoderParameters.MSI = Symbol.Barcode2.Design.DisabledEnabled.Default;
            this._barcodeService.Config.DecoderParameters.MSIParams.CheckDigitCount = Symbol.Barcode2.Design.CheckDigitCounts.Default;
            this._barcodeService.Config.DecoderParameters.MSIParams.CheckDigitScheme = Symbol.Barcode2.Design.CheckDigitSchemes.Default;
            this._barcodeService.Config.DecoderParameters.MSIParams.Redundancy = true;
            this._barcodeService.Config.DecoderParameters.MSIParams.ReportCheckDigit = false;
            this._barcodeService.Config.DecoderParameters.UPCA = Symbol.Barcode2.Design.DisabledEnabled.Default;
            this._barcodeService.Config.DecoderParameters.UPCAParams.Preamble = Symbol.Barcode2.Design.Preambles.Default;
            this._barcodeService.Config.DecoderParameters.UPCAParams.ReportCheckDigit = true;
            this._barcodeService.Config.DecoderParameters.UPCE0 = Symbol.Barcode2.Design.DisabledEnabled.Default;
            this._barcodeService.Config.DecoderParameters.UPCE0Params.ConvertToUPCA = false;
            this._barcodeService.Config.DecoderParameters.UPCE0Params.Preamble = Symbol.Barcode2.Design.Preambles.Default;
            this._barcodeService.Config.DecoderParameters.UPCE0Params.ReportCheckDigit = false;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.ImagerSpecific.AimDuration = -1;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.ImagerSpecific.AimMode = Symbol.Barcode2.Design.AIM_MODE.AIM_MODE_DEFAULT;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.ImagerSpecific.AimType = Symbol.Barcode2.Design.AIM_TYPE.AIM_TYPE_DEFAULT;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.ImagerSpecific.BeamTimer = -1;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.ImagerSpecific.DPMMode = Symbol.Barcode2.Design.DPM_MODE.DPM_MODE_DEFAULT;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.ImagerSpecific.FocusMode = Symbol.Barcode2.Design.FOCUS_MODE.FOCUS_MODE_DEFAULT;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.ImagerSpecific.FocusPosition = Symbol.Barcode2.Design.FOCUS_POSITION.FOCUS_POSITION_DEFAULT;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.ImagerSpecific.IlluminationMode = Symbol.Barcode2.Design.ILLUMINATION_MODE.ILLUMINATION_DEFAULT;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.ImagerSpecific.ImageCaptureTimeout = -1;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.ImagerSpecific.ImageCompressionTimeout = -1;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.ImagerSpecific.Inverse1DMode = Symbol.Barcode2.Design.INVERSE1D_MODE.INVERSE_DEFAULT;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.ImagerSpecific.LinearSecurityLevel = Symbol.Barcode2.Design.LINEAR_SECURITY_LEVEL.SECURITY_DEFAULT;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.ImagerSpecific.PicklistMode = Symbol.Barcode2.Design.PICKLIST_MODE.PICKLIST_DEFAULT;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.ImagerSpecific.PointerTimer = -1;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.ImagerSpecific.PoorQuality1DMode = Symbol.Barcode2.Design.DisabledEnabled.Default;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.ImagerSpecific.VFFeedback = Symbol.Barcode2.Design.VIEWFINDER_FEEDBACK.VIEWFINDER_FEEDBACK_DEFAULT;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.ImagerSpecific.VFFeedbackTime = -1;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.ImagerSpecific.VFMode = Symbol.Barcode2.Design.VIEWFINDER_MODE.VIEWFINDER_MODE_DEFAULT;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.ImagerSpecific.VFPosition.Bottom = 0;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.ImagerSpecific.VFPosition.Left = 0;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.ImagerSpecific.VFPosition.Right = 0;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.ImagerSpecific.VFPosition.Top = 0;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.LaserSpecific.AimDuration = -1;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.LaserSpecific.AimMode = Symbol.Barcode2.Design.AIM_MODE.AIM_MODE_DEFAULT;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.LaserSpecific.AimType = Symbol.Barcode2.Design.AIM_TYPE.AIM_TYPE_DEFAULT;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.LaserSpecific.BeamTimer = -1;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.LaserSpecific.BeamWidth = Symbol.Barcode2.Design.BEAM_WIDTH.DEFAULT;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.LaserSpecific.BidirRedundancy = Symbol.Barcode2.Design.DisabledEnabled.Default;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.LaserSpecific.ControlScanLed = Symbol.Barcode2.Design.DisabledEnabled.Default;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.LaserSpecific.DBPMode = Symbol.Barcode2.Design.DBP_MODE.DBP_DEFAULT;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.LaserSpecific.KlasseEinsEnable = Symbol.Barcode2.Design.DisabledEnabled.Default;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.LaserSpecific.LinearSecurityLevel = Symbol.Barcode2.Design.LINEAR_SECURITY_LEVEL.SECURITY_DEFAULT;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.LaserSpecific.PointerTimer = -1;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.LaserSpecific.RasterHeight = -1;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.LaserSpecific.RasterMode = Symbol.Barcode2.Design.RASTER_MODE.RASTER_MODE_DEFAULT;
            this._barcodeService.Config.ReaderParameters.ReaderSpecific.LaserSpecific.ScanLedLogicLevel = Symbol.Barcode2.Design.DisabledEnabled.Default;
            this._barcodeService.Config.ScanParameters.BeepFrequency = 2670;
            this._barcodeService.Config.ScanParameters.BeepTime = 200;
            this._barcodeService.Config.ScanParameters.CodeIdType = Symbol.Barcode2.Design.CodeIdTypes.Default;
            this._barcodeService.Config.ScanParameters.LedTime = 3000;
            this._barcodeService.Config.ScanParameters.ScanType = Symbol.Barcode2.Design.SCANTYPES.Default;
            this._barcodeService.Config.ScanParameters.WaveFile = "";
            this._barcodeService.DeviceType = Symbol.Barcode2.DEVICETYPES.FIRSTAVAILABLE;
            this._barcodeService.EnableScanner = false;
            this.ResumeLayout(false);

        }
        private void InitializeRegisteredActions()
        {
            _registeredActions = new Dictionary<string, Action>();
            _registeredActions.Add("EnableScanner", EnableScanner);
            _registeredActions.Add("DisableScanner", DisableScanner);

        }


        #endregion

        #region Cleanup

        private void BarcodeEnabledWebBrowser_Disposed(object sender, EventArgs e)
        {
            Navigating -= BarcodeEnabledWebBrowser_Navigating;
            Disposed -= BarcodeEnabledWebBrowser_Disposed;
            _barcodeService.EnableScanner = false;
        }

        #endregion

        private void barcodeService_OnScan(Symbol.Barcode2.ScanDataCollection scanDataCollection)
        {
            var scanData = scanDataCollection.GetFirst;
            string scanText = null;
            if (scanData.Result == Symbol.Barcode2.Results.SUCCESS && scanData.IsText)
            {
                scanText = scanData.Text;
            }

            if (scanText == null)
                return;

            CallJSMethod("OnScan", scanText);
        }

        private void BarcodeEnabledWebBrowser_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            var str = e.Url.ToString();

            if (str.Contains("call-sharp"))
            {
                var split = str.Split(':');
                var method = split[1];

                if (!_registeredActions.ContainsKey(method))
                {
                    NoMappedAction(method);
                }
                else
                {
                    _registeredActions[method]();
                }
                e.Cancel = true;
            }
        }

        #region JS API

        private void NoMappedAction(string called)
        {
            MessageBox.Show("No method " + called + " was registered", "Error", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
        }

        public void EnableScanner()
        {
            _barcodeService.EnableScanner = true;
        }

        public void DisableScanner()
        {
            _barcodeService.EnableScanner = false;
        }

        private void CallJSMethod(string methodName, params string[] args)
        {
            StringBuilder bld = new StringBuilder();
            bld.Append("javascript:");
            bld.Append(methodName);
            bld.Append('(');
            for (int i = 0; i < args.Length; i++)
            {
                bld.Append('\'');
                bld.Append(args[i]);
                bld.Append('\'');

                if (i + 1 < args.Length)
                    bld.Append(',');
            }
            bld.Append(')');

            var finalUri = new Uri(bld.ToString());

            Navigate(finalUri);
        }

        #endregion

    }
}
