﻿function EnableScanner() {
    CallExternal("EnableScanner");
}

function DisableScanner() {
    CallExternal("DisableScanner");
}

function CallExternal(methodName) {
    document.location = "call-sharp:" + methodName;
}