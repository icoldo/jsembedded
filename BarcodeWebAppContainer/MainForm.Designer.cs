﻿namespace BarcodeWebAppContainer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.barcodeEnabledWebBrowser1 = new BarcodeWebAppContainer.BarcodeEnabledWebBrowser();
            this.SuspendLayout();
            // 
            // barcodeEnabledWebBrowser1
            // 
            this.barcodeEnabledWebBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.barcodeEnabledWebBrowser1.Location = new System.Drawing.Point(0, 0);
            this.barcodeEnabledWebBrowser1.Name = "barcodeEnabledWebBrowser1";
            this.barcodeEnabledWebBrowser1.Size = new System.Drawing.Size(638, 455);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(638, 455);
            this.Controls.Add(this.barcodeEnabledWebBrowser1);
            this.Name = "MainForm";
            this.Text = "Dedeman procesare Comenzi";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        private BarcodeEnabledWebBrowser barcodeEnabledWebBrowser1;
    }
}

